function ParrotController($scope){
  $scope.parrots = [
  		[
  			{
  				'name': 'title',
  				'label': 'Название',
  				'value': 'parrot 1'
  			},
  			{
  				'name': 'population',
  				'label': 'Популяция',
  				'value': '30'
  			},
  			{
  				'name': 'color',
  				'label': 'Цвет',
  				'value': 'желтый'
  			},
  		],
  		[
  			{
  				'name': 'title',
  				'label': 'Название',
  				'value': 'parrot 2'
  			},
  			{
  				'name': 'population',
  				'label': 'Популяция',
  				'value': '50'
  			},
  			{
  				'name': 'color',
  				'label': 'Цвет',
  				'value': 'желтый'
  			},
  		],
  ];
}